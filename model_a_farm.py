class Animals:
    destinis = "I will end up on your plate"

    def __init__(self, name, color, age):
        self.name = name
        self.color = color
        self.age = age

    def activity(self, activity = None):
        if activity == None:
            return f"The {self.name} is lying upside down"
        return f"The {self.name} is {activity}"

    def voice(self, sound):
        return f"{self.name} says {sound}."

    def eat(self, eat = None):
        if eat == None:
            return f"The ={self.name} has full belly"
        return f"The {self.name} eat {eat}"

class Pig(Animals):

    def activity(self, activity = "bath" ):
        return f"The pig {self.name} like to be soak in the mud."

    def voice(self, sound = "Oink, Oink"):
        return super().voice(sound)

    def number_legs(self, number):
        return super().number_legs(number)

class Chicken(Animals):

    def run(self):
        return f"The chicken {self.name} like to running around without the reason."

    def voice(self, sound = "Ko Ko Ko"):
        return super().voice(sound)

    def number_legs(self, number = 2):
        return f"The chicken {self.name} has {number} legs."

class Cow(Animals):

    def eat(self):
        return f"The cow {self.name} love to eat grass all day."

    def voice(self, sound = "Mu Mu Mu"):
        return super().voice(sound)

renata = Pig("Renata", "pink", 8)
hubert = Chicken("Hubert", "ginger", 3)
mariolka = Cow("Mariolka", "black and white", 2)

print(f"This is our pig {renata.name}")
print(f"She is {renata.age} months old")
print(renata.activity())
print(renata.voice())
print(renata.destinis)
print()
print(f"This is our chicken {hubert.name} he has {hubert.color} color.")
print(hubert.run())
print(hubert.voice())
print(hubert.number_legs())
print()
print(f"This is our cow {mariolka.name} she is {mariolka.age} years old.")
print(mariolka.eat())
print(mariolka.voice())
print(mariolka.destinis)
