from pathlib import Path

documents_dir = Path.home() / "documents"
images_dir = Path.home() / "documents" / "images"
images_dir.mkdir(exist_ok=True)

for file in documents_dir.rglob("image*.*"):
    file.replace(images_dir / file.name)
