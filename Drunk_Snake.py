import pygame, random
from pygame.math import Vector2

class Beer:
    def __init__(self):
        self.random_positions()
          
    def draw_beer(self):
        beer_rect = pygame.Rect(int(self.pos.x * cell_size), int(self.pos.y * cell_size), cell_size, cell_size)
        window.blit(beer_pic, beer_rect)
        

    def random_positions(self):
        self.x = random.randint(0, cell_number -1)
        self.y = random.randint(0, cell_number -1)
        self.pos = Vector2(self.x, self.y)

class Snake: 
    def __init__(self):
        self.body = [Vector2(10, 12), Vector2(10, 13), Vector2(10, 14)]
        self.direction = Vector2(0, 0)
        self.new_block = False
        self.burp_sound = pygame.mixer.Sound("Sound/burp.mp3")
    
    def draw_snake(self):
        for block in self.body:
            block_rect = pygame.Rect(int(block.x * cell_size), int(block.y * cell_size), cell_size, cell_size)
            pygame.draw.rect(window, ("darkseagreen"), block_rect)

    def snake_move(self):
        if self.new_block == True:
            snake_body_copy = self.body[:]
            snake_body_copy.insert(0, snake_body_copy[0] + self.direction)
            self.body = snake_body_copy[:]
            self.new_block = False
        else:
            snake_body_copy = self.body[:-1]
            snake_body_copy.insert(0, snake_body_copy[0] + self.direction)
            self.body = snake_body_copy[:]

    def add_beer(self):
        self.new_block = True

    def play_sound(self):
        self.burp_sound.play()

    def game_over(self):
        self.body = [Vector2(10, 10), Vector2(10, 11), Vector2(10, 12)]


class Main: 
    def __init__(self):
        self.snake = Snake()
        self.beer = Beer()
    
    def update(self):
        self.snake.snake_move()
        self.get_beer()
        self.collision_object()

    def draw_game(self):
        self.beer.draw_beer()
        self.snake.draw_snake()
        self.draw_score()

    def get_beer(self):
        if self.beer.pos == self.snake.body[0]:
            self.beer.random_positions()
            self.snake.add_beer()
            self.snake.play_sound()

        for block in self.snake.body[1:]:
            if block == self.beer.pos:
                self.beer.random_positions()

    def collision_object(self):
        if not 0 <= self.snake.body[0].x < cell_number or not 0 <= self.snake.body[0].y < cell_number:
            self.end_game()
        
        for block in self.snake.body[1:]:
            if block == self.snake.body[0]:
                self.end_game()

    def draw_score(self):
        score_text ="      Score: " + str(len(self.snake.body) - 3)
        score_surface = score_font.render(score_text, True, ("lightseagreen"))
        score_x = 20
        score_y = 10
        score_rect = score_surface.get_rect(center = (score_x, score_y))
        window.blit(score_surface, score_rect) 

    def end_game(self):
        self.snake.game_over()

    
    



pygame.init() 
cell_size = 20
cell_number = 20
window = pygame.display.set_mode((400, 400)) 
pygame.display.set_caption("Drunk Snake Game")
clock = pygame.time.Clock()
score_font = pygame.font.Font(None, 25)
beer_pic = pygame.image.load("Graphics/cerveza.png").convert_alpha() 
beer = Beer()
snake = Snake()

window_upd = pygame.USEREVENT
pygame.time.set_timer(window_upd, 125)

main_game = Main()
         
surface_bg = pygame.Surface((800, 600))
surface_bg.fill("aliceblue")

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == window_upd:
            main_game.update()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if main_game.snake.direction.y != 1:
                    main_game.snake.direction = Vector2(0, -1)
            if event.key == pygame.K_DOWN:
                if main_game.snake.direction.y != -1:
                    main_game.snake.direction = Vector2(0, 1)
            if event.key == pygame.K_RIGHT:
                if main_game.snake.direction.x != -1:
                    main_game.snake.direction = Vector2(1, 0)
            if event.key == pygame.K_LEFT:
                if main_game.snake.direction.x != 1:
                    main_game.snake.direction = Vector2(-1, 0)


    window.blit(surface_bg, (0, 0))
    main_game.draw_game()
    pygame.display.update() 
    clock.tick(60) 