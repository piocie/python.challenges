from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

def get_text(page):
    return page.extractText()

pdf_path = (
    Path.home() /
    "practice_files" /
    "exercise" /
    "scrambled.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

pages = list(pdf_reader.pages)
pages.sort(key=get_text)

for page in pages:
    rotation_degrees = page["/Rotate"]
    if rotation_degrees != 0:
        page.rotateCounterClockwise(rotation_degrees)
    pdf_writer.addPage(page)

with Path("unscrambled.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
    
