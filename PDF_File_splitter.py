from pathlib import Path
from PyPDF2 import PdfFileWriter, PdfFileReader

class PdfFileSplitter:

    def __init__(self, pdf_path):
        self.pdf_reader = PdfFileReader(pdf_path)

    def split(self, breakpoint):
        self.writer1 = PdfFileWriter()
        self.writer2 = PdfFileWriter()

        for page in self.pdf_reader.pages[:breakpoint]:
            self.wrtiter1.addPage(page)

        for page in self.pdf_reader.pages[breakpoint:]:
            self.writer2.addPage(page)

    def write(self, filename):

        with Path(filename + "_1.pdf").open(mode="wb") as output_1:
            self.writer1.write(output_1)

        with Path(filename + "_2.pdf").open(mode="wb") as output_2:
            self.writer2.writer(output_2)

pdf_splitter = PdfFileSplitter("Users/44787/practice_files/Pride_and_Prejudice.pdf")
pdf_splitter.split(150)
pdf_splitter.write("Pride_and_Prejudice_split")
    
        
    
