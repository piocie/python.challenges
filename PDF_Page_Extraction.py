import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter

input_path = gui.fileopenbox(
    title="Select a PDF file",
    default="*.pdf"
)
if input_path is None:
    exit()

start_page = gui.enterbox(
    msg="Choose the first page",
    title="Where to start"
)
if start_page is None:
    exit()

input_file = PdfFileReader(input_path)
total_pages = input_file.getNumPages()

while start_page == "0" or int(start_page) > total_pages:
    gui.msgbox(
        msg="Please put valid page number"
    )
    start_page = gui.enterbox(
    msg="Choose the first page",
    title="Where to start"
    )

if start_page is None:
    exit()

end_page = gui.enterbox(
    msg="Enter the last page to use"
)

while end_page == "0" or int(end_page) > total_pages:
    gui.msgbox(
        msg="Please put valid page number"
    )
    end_page = gui.enterbox(
    msg="Enter the last page to use"
    )
if end_page is None:
    exit()  

output_path = gui.filesavebox(
    msg="Choose a location to save file:",
    default="*.pdf"
)
if output_path is None:
    exit()

while input_path == output_path:
    gui.msgbox(
        msg="You cannot overwrite file !!!"
    )
    output_path = gui.filesavebox(
    msg="Choose a location to save file:"
    )
if output_path is None:
    exit()

new_pdf = PdfFileWriter()

for total_page in range(int(start_page), int(end_page)):
    page = input_file.getPage(total_page)
    new_pdf.addPage(page)

with open(output_path, "wb") as output_file:
    new_pdf.write(output_file)

for i in range(int(start_page) - 1, int(end_page)):
    print(i)
