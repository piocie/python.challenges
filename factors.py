user_input = int(input("Enter a positive integer: "))

for number in range(1, user_input + 1):
    if user_input % number == 0:
        print(f"{number} is a factor of {user_input}")
