def bubble_sort(numbers):
    for i in range(len(numbers)):
        for n in range(len(numbers) - i - 1):
            if numbers[n] > numbers[n + 1]:
                numbers[n], numbers[n + 1] = numbers[n + 1], numbers[n]
                
    print(numbers)


numbers = [6, 2, 1, 3, 5, 4]    

bubble_sort(numbers)
