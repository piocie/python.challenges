import random

nouns = [
    "fossil", "horse", "aardvark", "judge", "chef", "mango",
    "extrovert", "gorilla"
]
verbs = [
    "kicks", "jingles", "bounces", "slurps", "meows",
    "explodes", "curdles"
]
adjectives = [
    "furry", "balding", "incredulous", "fragrant",
    "exuberant", "glistening"
]
prepositions =[
    "against", "after", "into", "beneath", "upon",
    "for", "in", "like", "over", "within"
]
adverbs = [
    "curiously", "extravagantly", "tantalizingly",
    "furiously", "sensuously"
]

vowels = ["a", "e", "i", "o", "u"]


def random_words(array ,count):
    words = []
    for i in range(count):
        random_word = random.choice(array)
        words.append(random_word)
    return words


def article(word):
    if word[0] in vowels:
        return "An"
    else:
        return "A"


my_nouns = random_words(nouns, 3)
my_verbs = random_words(verbs, 3)
my_adjectives = random_words(adjectives, 3)
my_prepositions = random_words(prepositions, 2)
my_adverbs = random_words(adverbs, 1)
first_art = article(my_adjectives[0])
second_art = article(my_adjectives[2])

print(f"{first_art} {my_adjectives[0]} {my_nouns[0]}")

print(f"{first_art} {my_adjectives[0]} {my_nouns[0]} {my_verbs[0]} {my_prepositions[0]} the {my_adjectives[1]} "
      f"{my_nouns[1]}")
print(f"{my_adverbs[0]}, the {my_nouns[0]} {my_verbs[1]}")
print(f"the {my_nouns[1]} {my_verbs[2]} {my_prepositions[1]} {second_art} {my_adjectives[2]} {my_nouns[2]}")