import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

keys_list = list(capitals_dict.keys())
random_state = random.choice(keys_list)
random_capitol_city = capitals_dict[random_state]
flag = False

while not flag:
    question = input(f"What is the capital of {random_state} ?, Your answer ===>> ").lower()
    if question == random_capitol_city.lower():
        print("Correct")
        flag = True
    elif question == "exit":
        print(f"Correct answer is {random_capitol_city}, Goodbye")
        flag = True





