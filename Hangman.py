word = input("Welcome in Hangman game !!!\n\nPlease choose the world to guess: ")

guesses = []
allowed_errors = 7
condition = True

while condition:
    for letter in word:
        if letter.lower() in guesses:
            print(letter, end = " ")
        else:
            print("_", end = " ")
    print("") 

    guess = input("Quess the letter:  ")
    guesses.append(guess.lower())
    if guess.lower() not in word.lower():
        allowed_errors -= 1
        if allowed_errors==6:
            print()
            print()
            print()
            print()
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 5:
            print()
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 4:
            print()
            print("________________")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 3:
            print()
            print("_________________")
            print("|               |")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 2:
            print()
            print("_________________")
            print("|               |")
            print("|               @")
            print("|")
            print("|")
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 1:
            print()
            print("_________________")
            print("|               |")
            print("|               @")
            print("|               |")
            print("|               |")
            print("|")
            print("|")
            print("|")
            print()
        elif allowed_errors == 0:
            print()
            print("_________________")
            print("|               |")
            print("|               @")
            print("|              /|\\")
            print("|               |")
            print("|              / \\")
            print("|")
            print("|")
            print()
            print(f"GAME OVER  It was {word}!")
            break

    condition = False
    for letter in word:
        if letter.lower() not in guesses:
            condition = True
            
if condition == False:
    print(F"Congratulation you WIN, you found the word: {word} :)")


