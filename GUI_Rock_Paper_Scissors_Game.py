import tkinter as tk
import random

player_score = 0
opponent_score = 0
player_choice = ""
opponent_choice = ""

def rnd_opponent_choice():
    return random.choice(["Rock", "Paper", "Scissor"])

def choice_to_number(choice):
    rps = {"Rock":0, "Paper":1, "Scissor":2}
    return rps[choice]

def number_to_choice(number):
    rps = {0:"Rock", 1:"Paper", 2:"Scissor"}
    return rps[number]

def result(human_choice, comp_choice):
    global player_score
    global opponent_score
    user = choice_to_number(human_choice)
    comp = choice_to_number(comp_choice)
    if user == comp:
        print("Draw")
    elif(user-comp) % 3 == 1:
        print("Victory !!!")
        player_score += 1
    else:
        print("Opponent wins")
        opponent_score += 1
    score_display = tk.Text(master=window, height=15, width=46, bg="cyan")
    score_display.grid(column=0, row=4)
    answer = f"Your Choice: {player_choice} \n\nOpponent's Choice : {opponent_choice} \n\n Your Score : {player_score} \n\n Opponent Score : {opponent_score} "
    score_display.insert(tk.END, answer)

def rock():
    global player_choice
    global opponent_choice
    player_choice = "Rock"
    opponent_choice = rnd_opponent_choice()
    result(player_choice, opponent_choice)
    
def paper():
    global player_choice
    global opponent_choice
    player_choice = "Paper"
    opponent_choice = rnd_opponent_choice()
    result(player_choice, opponent_choice)
    
def scissor():
    global player_choice
    global opponent_choice
    player_choice = "Scissor"
    opponent_choice = rnd_opponent_choice() 
    result(player_choice, opponent_choice)

window = tk.Tk()
window.geometry("370x300")
window.title("Let's play Rock Paper Scissors Game !!!")

btn_1 = tk.Button(text="ROCK", width= "52", command=rock, bg="grey")
btn_1.grid(column=0,row=1)
btn_2 = tk.Button(text="PAPER", width= "52", command=paper, bg="white")
btn_2.grid(column=0,row=2)
btn_3 = tk.Button(text="SCISSOR", width= "52", command=scissor, bg="silver")
btn_3.grid(column=0, row=3)

window.mainloop()
