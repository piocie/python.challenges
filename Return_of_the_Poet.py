import tkinter as tk
import random
from tkinter import filedialog

window = tk.Tk()
window.title("Make your poem!")

main_frame = tk.Frame()
main_frame_lbl = tk.Label(master=main_frame, text="Enter your favorite words, separated by commas.")
main_frame_lbl.pack()
main_frame.pack()

input_frame = tk.Frame()
label_noun = tk.Label(master=input_frame, text="Nouns: ")
label_verb = tk.Label(master=input_frame, text="Verbs: ")
label_adj = tk.Label(master=input_frame, text="Adjectives: ")
label_prep = tk.Label(master=input_frame, text="Prepositions: ")
label_adver = tk.Label(master=input_frame, text="Adverbs: ")

entry_noun = tk.Entry(input_frame, width=80)
entry_verb = tk.Entry(input_frame, width=80)
entry_adj = tk.Entry(input_frame, width=80)
entry_prep = tk.Entry(input_frame, width=80)
entry_adver = tk.Entry(input_frame, width=80)

label_noun.grid(row=2, column=1, sticky="e")
entry_noun.grid(row=2, column=2)
label_verb.grid(row=3, column=1, sticky="e")
entry_verb.grid(row=3, column=2)
label_adj.grid(row=4, column=1, sticky="e")
entry_adj.grid(row=4, column=2)
label_prep.grid(row=5, column=1, sticky="e")
entry_prep.grid(row=5, column=2)
label_adver.grid(row=6, column=1, sticky="e")
entry_adver.grid(row=6, column=2)

input_frame.pack(padx=10, pady=10)

button_frame = tk.Frame(master=window)
button_frame.pack()
button = tk.Button(master=button_frame, text="Generate")
button.pack()

bottom_frame = tk.Frame(master=window)
bottom_label = tk.Label(master=bottom_frame, text="Your poem: ")
ready_poem = tk.Label(master=bottom_frame, text="Press the 'Generate' button to display your poem.")
save_button = tk.Button(master=bottom_frame, text="Save to file")
bottom_frame.pack()
bottom_label.pack(pady=10)
ready_poem.pack(pady=10)
save_button.pack(pady=10)

window.mainloop()

def gen_poem():
    noun = entry_noun.get().split(",")
    verb = entry_verb.get().split(",")
    adjective = entry_adj.get().split(",")
    adverb = entry_adver.get().split(",")
    preposition = entry_prep.get().split(",")

    if len(noun) < 3 or len(verb) < 3 or len(adjective) < 3 or len(preposition) < 2 or len(adverb) < 1:
        ready_poem["text"] = ("Please enter the correct number of words")

        return

    noun1 = random.choice(noun)
    noun2 = random.choice(noun)
    noun3 = random.choice(noun)

    while noun1 == noun2:
        noun2 = random.choice(noun)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(noun)

    verb1 = random.choice(verb)
    verb2 = random.choice(verb)
    verb3 = random.choice(verb)
    
    while verb1 == verb2:
        verb2 = random.choice(verb)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verb)

    adj1 = random.choice(adjective)
    adj2 = random.choice(adjective)
    adj3 = random.choice(adjective)
    
    while adj1 == adj2:
        adj2 = random.choice(adjective)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjective)

    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)
    
    while prep1 == prep2:
        prep2 = random.choice(preposition)

    adv1 = random.choice(adverb)

    new_poem = f"{article} {adj1} {noun1}\n\n"
    new_poem = new_poem + f"{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
    new_poem = new_poem + f"{adv1}, the {noun1} {verb2}\n"
    new_poem = new_poem + f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"
    ready_poem["text"] = new_poem

def save_file():
    type_list = [("Text files", "*.txt")]
    file_name = filedialog.asksaveasfilename(
        filetypes=type_list, defaultextension="*.txt"
    )
    
    if file_name != "":
        with open(file_name, "w") as output_file:
            output_file.writelines(ready_poem["text"])


button["command"] = gen_poem
save_button["command"] = save_file

window.mainloop()
