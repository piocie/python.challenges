cat_with_hat = [False] * 101
final_cats_with_hats = []

for lap in range(1, 101):
    for cat in range(1, 101):
        if cat % lap == 0:
            if cat_with_hat[cat]:
                cat_with_hat[cat] = False
            else:
                cat_with_hat[cat] = True

for i in range(1, 101):
    if cat_with_hat[i]:
        final_cats_with_hats.append(i)

print(final_cats_with_hats)
